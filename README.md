 .SYNOPSIS
  
  Script check external IP using https://ipify.org and compare them with set IP. If it is not equal, than script turn off all network adapters.
  
  .DESCRIPTION

  Script MUST be run with administrative privilage, otherwise it can`t turn off network adapters
  All settings set in settings.ini
  Script runing in endless cycle, but you can specifies parametr to change it.
  Script generate log file, care about it rotate by your own
  If you using scheduler to run this script, dont forget to set working directory in task, othervise script cant write logs.


  .PARAMETER check_count

  Specifies count of checks. If this parametr is defined, than script run count check and exit.

  .OUTPUTS

  Script generate message when detects than set IP adress in not eqal current external IP
  
  .EXAMPLE

  PS> check_ip.ps1

  .EXAMPLE
  
  PS> check_ip.ps1 -check_count 5
