<#
  .VERSION
  1.1
  .SYNOPSIS
  Script check external IP using https://ipify.org and compare them with set IP. If it is not equal, than script turn off all network adapters.
  
  .DESCRIPTION
  Script MUST be run with administrative privilage, otherwise it can`t turn off network adapters
  All settings set in settings.ini
  Script runing in endless cycle, but you can specifies parametr to change it.
  Script generate log file, care about it rotate by your own
  If you using scheduler to run this script, dont forget to set working directory in task, othervise script cant write logs.


  .PARAMETER check_count
  Specifies count of checks. If this parametr is defined, than script run count check and exit.

  .OUTPUTS
  Script generate message when detects than set IP adress in not eqal current external IP
  
  .EXAMPLE
  PS> check_ip.ps1

  .EXAMPLE
  PS> check_ip.ps1 -check_count 5
#>

Param(
    [int32]$check_count = 0
)

#Start-Transcript "C:\_external ip monitor\log.txt"

function Get-IniFile 
{  
    param(  
        [parameter(Mandatory = $true)] [string] $filePath  
    )  
    
    $anonymous = "NoSection"
  
    $ini = @{}  
    switch -regex -file $filePath  
    {  
        "^\[(.+)\]$" # Section  
        {  
            $section = $matches[1]  
            $ini[$section] = @{}  
            $CommentCount = 0  
        }  

        "^(;.*)$" # Comment  
        {  
            if (!($section))  
            {  
                $section = $anonymous  
                $ini[$section] = @{}  
            }  
            $value = $matches[1]  
            $CommentCount = $CommentCount + 1  
            $name = "Comment" + $CommentCount  
            $ini[$section][$name] = $value  
        }   

        "(.+?)\s*=\s*(.*)" # Key  
        {  
            if (!($section))  
            {  
                $section = $anonymous  
                $ini[$section] = @{}  
            }  
            $name,$value = $matches[1..2]  
            $ini[$section][$name] = $value  
        }  
    }  

    return $ini  
}  

function WriteLog
{
Param ([string]$LogString)

$Stamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
$LogMessage = "$Stamp $LogString"
Add-content $LogFile -value $LogMessage
}

function Wait
{
Param ([DateTime] $start_date)

$time_of_run = New-TimeSpan -Start $start_date -End $(Get-Date)
    $wait_time = $check_period - $time_of_run.Seconds
    if ($wait_time -gt 0)
    {
        Start-Sleep -Seconds $wait_time
    }
}



$iniFile = Get-IniFile .\settings.ini

#set varibles
$logfile = $iniFile.log.file
$set_ip_adress = $iniFile.main.ip_adress
$check_period = $iniFile.main.check_period
$current_ip_adress = ''

#start endless cycle
$cycle_count = 0
While ($true)
{
    $cycle_count++
    $start_date = Get-Date

    #find current external ip adress
    try
    {
        
        $request = Invoke-RestMethod -Uri 'https://api.ipify.org?format=json' -TimeoutSec 30 -ErrorAction Stop
		$current_ip_adress = $request.ip
    }
    catch
    {
        
	    #todo: log this mess
        WriteLog 'Cant recive current ip adress'
	    Write-Output 'Cant recive current ip adress'
        Write-Output $_

        #wait when check period is over and start again
        Wait($start_date)

        if ($($check_count -ne 0) -and ($($cycle_count + 1) -gt $check_count))
        {
            break
        }
        
        continue
    }
    #Write-Host "start equal"
    if ($current_ip_adress -ne $set_ip_adress)
    { 
    	Write-Output "IP adresses is NOT equal: current ip adress is $current_ip_adress but set ip adress is $set_ip_adress"
    	WriteLog "IP adresses is NOT equal: current ip adress is $current_ip_adress but set ip adress is $set_ip_adress"
    	#shutdown all network adapter and wait for admin reaction
    	Get-NetAdapter|Disable-NetAdapter -Confirm:$false
    
    }
    #wait when check period is over and start again
    Wait($start_date)
    if ($($check_count -ne 0) -and ($($cycle_count + 1) -gt $check_count))
    {
        break
    }
}